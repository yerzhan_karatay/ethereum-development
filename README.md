# ethereum-development repository guide

## Chainlist
Sample Ethereum Dapp to create your classifieds on Ethereum.

## ChainSkills
Training truffle skills.

## Exchange
This is a fully distributed exchange with truffle and MetaMask on the Rinkeby-Testnet.
The Token-Exchange can be tested with MetaMask and the Rinkeby Test-Net.

## MetaCoin
Example webpack project with Truffle. Includes contracts, migrations, tests, user interface and webpack build pipeline.

## PetsShop
Sample Ethereum Dapp to create pets shop on Ethereum.

## Private-testnet
Private test network for apps.

## TrainingByOfficialDocumentation
Some codes from ethereum documentation.

# About me
If you are interested about my work, what I do etc: [LinkedIn](https://www.linkedin.com/in/yerzhan-karatayev/) [Facebook](https://www.facebook.com/yerzhan.kst)