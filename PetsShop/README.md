# PetsShop - Your DAPP on Ethereum

Sample Ethereum Dapp to create pets shop on Ethereum.

![PetsShop distributed app screen](PetsShop.png "PetsShop overview page")

Follow the steps described below to install, deploy and run the Dapp.

## Warning
**Make sure you don't run tests on Ethereum's main net otherwise you will use real ether with no chance to get them back**

## Prerequisites: Install tools and frameworks

To build, deploy and test your Dapp locally, you need to install the following tools and frameworks:
* **node.js and npm**: https://nodejs.org/en/
  * Node.js can be installed using an installation package or through some package managers like Homebrew on a Mac.

* **GIT**: https://git-scm.com/
  * Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency.

* **Ganache**: http://truffleframework.com/ganache
  * Simulates an Ethereum node.

* **Metamask**: https://metamask.io/
  * Transforms Chrome into a Dapp browser

## Step 1. Clone the project

```
git clone https://gitlab.com/yerzhan_karatay/ethereum-development/
cd PetsShop
```

## Step 2. Install all dependencies

```
$ npm install truffle
```

## Step 3. Compile the smart contract.

```
$ truffle compile
```

## Step 4. Start ganache

 If you haven't already, [download Ganache](http://truffleframework.com/ganache) and double click the icon to launch the application. This will generate a blockchain running locally on port 7545.

## Step 5. Migrate and test your smart contract

```
$ truffle migrate
$ truffle test
```

## Step 6. Metamask: connect to your local Ethereum node

Unlock the Metamask extension in Chrome, and switch it to the network "Localhost 8545".

## Step 7. Metamask: import your accounts

Import accounts defined in your Ganache Ethereum node.

## Step 8. Run your frontend application

```
$ npm run dev
```

In your browser, open the following URL: http://localhost:3000

## Step 9. Metamask: switch to the ganache account

When you switch accounts or networks in Metamask, you have to refresh your  page to let your frontend application know about it.

## Step 10. Adopt pets

You can adopt pets using accounts imported in Metamask.

Metamask will ask you to confirm the transaction before adopt pets.

# About me
If you are interested about my work, what I do etc: [LinkedIn](https://www.linkedin.com/in/yerzhan-karatayev/) [Facebook](https://www.facebook.com/yerzhan.kst)