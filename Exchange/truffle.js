module.exports = {
  networks: {
    development: {
      host: "localhost",
      port: 8545,
      network_id: "*" // Match any network id
    },
    rinkeby: {
      host: "localhost",
      port: 8545,
      network_id: "4", // Rinkeby testnet 4
      from: "0xE1Cf6Ea1633f0958aF30399861A5555A2B350A94",
      gas: 4700000
    }
  }
};
