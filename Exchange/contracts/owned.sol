pragma solidity ^0.4.0;

/**
 * The owned contract does this and that...
 */
contract owned {
    address owner;

    modifier onlyowner() { 
        if (msg.sender == owner) {
            _; 
        }
    }

    function owned () {
        owner = msg.sender;
    }    
}
